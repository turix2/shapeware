suppressPackageStartupMessages(library(plyr))
suppressPackageStartupMessages(library(dplyr))
suppressPackageStartupMessages(library(ggplot2))

args <- commandArgs(TRUE)

plot_coverage <- function(input.file.list, linegraph.file, sample.names, agg.csv.file) {


	for (i in 1:length(input.file.list)){

		temp.df <- read.table(input.file.list[i])
		names(temp.df) <- c('Sequence', 'Position', 'Base', 'Coverage', 'Mutation.Rate', 'Del.Discard.Count', 'Ins.Discard.Count')	
		temp.df$Sample <- sample.names[i]

		if (i == 1) {
			agg.df <- temp.df
		} else {
			agg.df <- rbind(agg.df, temp.df)
		}

	}

	plot.height <- 4 * length(unique(agg.df$Sequence))

	p <- ggplot(agg.df, aes(x = Position, y = Coverage, color = Sample)) +
	geom_line() +
	facet_wrap(~ Sequence, scales = 'free', ncol=1) +
	theme_bw()

	suppressWarnings(ggsave(linegraph.file, p, width = 10, height = plot.height, limitsize=FALSE))

	write.csv(agg.df, agg.csv.file, row.names=FALSE, quote=FALSE)

}

plot_coverage(unlist(snakemake@input), snakemake@output[[1]], snakemake@config[['sample_names']], snakemake@output[[2]])
