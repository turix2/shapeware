#!/usr/bin/env bash

# Helper script to install dependencies for SHAPEware

# See README.md for more information

cd "$(dirname "$0")"

conda_path=$(which conda)
rscript_path=$(which Rscript)
os_name=$(uname)

if [ ! -f $conda_path ]; then
	echo "conda binary not found. Conda is required for SHAPEware function. See the README file."
	exit 1
fi

if [ ! -f $rscript_path ]; then
	echo "Rscript binary not found. R is required for SHAPEware function. See the README file."
	exit 1
fi

if [ $os_name == "Linux" ]; then
	echo "Attempting to create shapeware environment..."
	conda env create -n shapeware --file conda/linux.yml

	if [ $? != 0 ]; then
		echo "First attempt failed. Attempting build with relaxed package version restrictions..."
		conda env create -n shapeware --file conda/linux_simple.yml

		if [ $? != 0 ]; then
			echo "Environment build failed. Required packages are no longer available or versions are incompatible."
			exit 1
		fi


	fi

elif [ $os_name == "Darwin" ]; then
	echo "Attempting to create shapeware environment..."
	conda env create -n shapeware --file conda/macos.yml

	if [ $? != 0 ]; then
		echo "First attempt failed. Attempting build with relaxed package version restrictions..."
		conda env create -n shapeware --file conda/macos_simple.yml

		if [ $? != 0 ]; then
			echo "Environment build failed. Required packages are no longer available or versions are incompatible."
			exit 1
		fi

	fi

else
	echo "OS $os_name not supported."

fi

echo "Checking R packages are installed..."
Rscript src/check_packages.R

r_exit_code=$?

if [ $r_exit_code != 0 ]; then
	echo "Required R packages were not installed. This will prevent SHAPEware from running."
	exit 1
fi

echo "Do you want to download the example files? (y/n)"

read dl_option

curl_path=$(command -v curl)
wget_path=$(command -v wget)

if [ $dl_option == 'y' ] || [ $dl_option == 'Y' ]; then

	if [ ! -f $curl_path ]; then
		echo "curl binary not found. curl or wget is required to download files."
		
	elif [ ! -f $wget_path ]; then
		echo "wget binary not found. curl or wget is required to download files."
		exit 1
	else
		example_data/download_files.sh
		echo "You should now be able to test SHAPEware with the following commands:"
		echo "source activate shapeware"
		echo "./analyze_shape-map.py example_data/"

	fi
fi

